# 小林研究室 Web サイト作成ツール #

### このリポジトリは何? ###

* 小林研究室の Web サイトを作るためのツールです。
* コンテンツも含みます
* [展開後のページ](http://www2.metro-cit.ac.jp/~hkob/)

### どのようにセットアップするの? ###

* 必要であれば rbenv などで新しい Ruby を入れてください。
* gem install bundler で bundle コマンドをインストールします
* 以下のコマンドで vendor/bundle に haml をインストールします
    bundle install --path vendor/bundle
* rake すると contents/haml のテンプレートデータを元に、contents/html の下にページが作成されます。

### コンテンツ管理方法は? ###

* site.yml でページ全体の設定を行います。
* make_haml.rb で haml テンプレートから haml を作成するので、共通部分を修正したい場合には、これを修正してください。
* contents/haml 内のテンプレートは本文のみ記述します。複数箇所で同じような表記を見つけたら、メソッド化し、make_haml.rb に吸収できないか考えてください。