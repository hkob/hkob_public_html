sudo ufw default deny # デフォルトでは通信を拒否
sudo ufw enable # ufw の設定を指定
sudo ufw allow http # http は許可
sudo ufw allow https # https も許可
sudo ufw allow 22 # ssh のポートを変えようと思ったが職場から繋がらなくなるのでそのままに
sudo ufw reload # ufw の設定を読み込み
