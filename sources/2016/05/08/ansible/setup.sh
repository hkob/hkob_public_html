#!/bin/sh
# homebrew のインストール
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
# command line tools のインストール
xcode-select --install
# /usr/local を自分の所有に変更
sudo chown -R $USER /usr/local
# brew の設定ができているかどうかを確認
brew doctor
# ansible を homebrew でインストール
brew install ansible

