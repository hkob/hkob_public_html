class Position
  def initialize(x, y)
    @x, @y = x, y
  end
  attr_reader :x, :y

  def move(dx, dy)
    @x += dx
    @y += dy
  end
end
class Circle
  def initialize(x, y, r)
    @p, @r = Position.new(x, y), r
  end
  attr_reader :p, :r

  def move(dx, dy)
    @p.move(dx, dy)
  end
end

class Rectangle
  def initialize(x, y, width, height)
    @p, @width, @height = Position.new(x, y), width, height
  end
  attr_reader :p, :width, :height

  def move(dx, dy)
    @p.move(dx, dy)
  end
end

class Triangle
  def initialize(x1, y1, x2, y2, x3, y3)
    @p1, @p2, @p3 = Position.new(x1, y1), Position.new(x2, y2), Position.new(x3, y3)
  end
  attr_reader :p1, :p2, :p3

  def move(dx, dy)
    @p1.move(dx, dy)
    @p2.move(dx, dy)
    @p3.move(dx, dy)
  end
end

def moveAll(objects, dx, dy)
  objects.each do |obj|
    obj.move(dx, dy)
  end
end

cir = Circle.new 100, 200, 300
rect = Rectangle.new 300, 400, 100, 200
tri = Triangle.new 10, 20, 30, 40, 10, 30
objects = [ cir, rect, tri ]

moveAll objects, 50.0, 30.0

print "circle = #{ cir.p.x }, #{ cir.p.y }, #{ cir.r }\n"
print "rect = #{ rect.p.x }, #{ rect.p.y }, #{ rect.width}, #{ rect.height }\n"
print "tri = #{ tri.p1.x }, #{ tri.p1.y }, #{ tri.p2.x }, #{ tri.p2.y }, #{ tri.p3.x }, #{ tri.p3.y }\n"
