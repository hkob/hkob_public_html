#include <stdio.h>

typedef struct { double x, y, r; } circle;
typedef struct { double x, y, width, height; } rectangle;
typedef struct { double x1, y1, x2, y2, x3, y3; } triangle;
enum { CIRCLE, RECTANGLE, TRIANGLE };
#define N 3

void moveCircle(circle *cp, double dx, double dy) {
    cp->x += dx; cp->y += dy;
}

void moveRectangle(rectangle *rp, double dx, double dy) {
    rp->x += dx; rp->y += dy;
}

void moveTriangle(triangle *tp, double dx, double dy) {
    tp->x1 += dx; tp->y1 += dy;
    tp->x2 += dx; tp->y2 += dy;
    tp->x3 += dx; tp->y3 += dy;
}

void moveAll(void **objects, int *types, int n, double dx, double dy) {
    int i;
    for (i = 0; i < n; i++) {
        void *obj = objects[i];
        switch (types[i]) {
            case CIRCLE:
                moveCircle((circle *)obj, dx, dy);
                break;
            case RECTANGLE:
                moveRectangle((rectangle *)obj, dx, dy);
                break;
            case TRIANGLE:
                moveTriangle((triangle *)obj, dx, dy);
                break;
            default:
                break;
        }
    }
}

int main() {
    circle cir = { 100, 200, 300 };
    rectangle rect = { 300, 400, 100, 200 };
    triangle tri = { 10, 20, 30, 40, 10, 30 };
    void *objects[N] = { &cir, &rect, &tri };
    int types[N] = { CIRCLE, RECTANGLE, TRIANGLE };

    moveAll(objects, types, N, 50.0, 30.0);
    printf("circle = %f, %f, %f\n", cir.x, cir.y, cir.r);
    printf("rect = %f, %f, %f, %f\n", rect.x, rect.y, rect.width, rect.height);
    printf("tri = %f, %f, %f, %f, %f, %f\n", tri.x1, tri.y1, tri.x2, tri.y2, tri.x3, tri.y3);
    return 0;
}
