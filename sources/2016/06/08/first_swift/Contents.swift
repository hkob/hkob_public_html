//: Playground - noun: a place where people can play

import UIKit

public class MyComplex {
    private var _real: Double
    private var _image: Double
    public var abs: Double { return sqrt(_real * _real + _image * _image) }

    public init(real: Double, image: Double) {
        _real = real
        _image = image
    }

    public func add(other: MyComplex) -> MyComplex {
        return MyComplex(real: _real + other._real, image: _image + other._image)
    }
}

public func + (lhs: MyComplex, rhs: MyComplex) -> MyComplex {
    return lhs.add(rhs)
}

let R = 500.0
let L = 100e-3 // 100[mH]
let C = 10e-6 // 10[μF]
var zr: MyComplex
var zl: MyComplex
var zc: MyComplex
var w: Double

var fs = (0 ... 100).map { i -> Double in
    Double(i) * 10
}

var z = fs.map { f -> Double in
    w = 2 * M_PI * f
    zr = MyComplex(real: 0.5, image: 0.0)
    zl = MyComplex(real: 0.0, image: w * L)
    zc = MyComplex(real: 0.0, image: -1.0 / (w * C))
    zr + zl + zc
    return (zr + zl + zc).abs
}



