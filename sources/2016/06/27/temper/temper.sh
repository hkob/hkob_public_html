#!/bin/sh
ANHOUR=60
ADAY=1440
AWEEK=10080

cd $HOME/temp
echo `date "+%H:%M %m/%d h%H d%d"` `/usr/local/bin/temp` >> all
/usr/bin/tail -n $AWEEK all > all_cut
/bin/mv all_cut all
/usr/bin/tail -n $ADAY all | awk 'BEGIN {pre=""; sum=0; count=0; }{ if ($4 == pre) { sum += $5; count++; } else { if (pre != "") { printf("-\t-\t-\t%s\t%f\n", pre, sum / count); } pre = $4; sum = $5; count = 1; }}END{printf("-\t-\t-\t%s\t%f\n", pre, sum / count); }' > aweek
/usr/bin/tail -n $ADAY all | awk 'BEGIN {pre=""; sum=0; count=0; }{ if ($3 == pre) { sum += $5; count++; } else { if (pre != "") { printf("-\t-\t%s\t-\t%f\n", pre, sum / count); } pre = $3; sum = $5; count = 1; }}END{printf("-\t-\t%s\t-\t%f\n", pre, sum / count); }' > aday
/usr/bin/tail -n $ANHOUR all > anhour

sh graph.sh
