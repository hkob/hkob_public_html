#!/bin/sh
ANHOUR=60
ADAY=1440
AWEEK=10080

cd $HOME/temp

/usr/local/bin/gnuplot <<EOL > /dev/null
set term png
set output '/Library/Server/Web/Data/Sites/Default/temp/anhour.png'
set xtics rotate by 90 offset 0,-1.0 left font ",6"
plot 'anhour' u 0:5:xtic(1) t 'an hour' w l
quit
EOL

/usr/local/bin/gnuplot <<EOL > /dev/null
set term png
set output '/Library/Server/Web/Data/Sites/Default/temp/aday.png'
set xtics rotate by 90 offset 0,-1.3,0 left font ",10"
plot 'aday' u 0:5:xtic(3) smooth unique t 'a day' w l
quit
EOL

/usr/local/bin/gnuplot <<EOL > /dev/null
set term png
set output '/Library/Server/Web/Data/Sites/Default/temp/aweek.png'
plot 'aweek' u 0:5:xtic(4) t 'a week' w l
quit
EOL

