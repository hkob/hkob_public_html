#! /usr/bin/env ruby

require 'pathname'

if ARGV.length == 1
  fname = ARGV.first
  open(fname) do |f|
#    title = f.gets
#    heading = f.gets

    print <<"EOL"
:ruby
  require './my_func'

  @fname = "#{ fname }"
  @fname_without_base = @fname.gsub('contents/haml/', '')
  @objs = YAML.load_file('site.yml')
  file_info = @objs['links'][@fname_without_base]
  unless file_info
    print "site.yml の links[" + @fname_without_base + "] が見つかりません\\n"
    exit(1)
  end
  title = file_info['name']

!!!
%html
  %head
    %meta(charset='UTF-8')
    %meta(name="viewport" content="width=device-width,initial-scale=1")
    %link{ @objs['bootswatch_css'] }
    %link{ @objs['highlight_css'] }
    %script{ @objs['jquery_js'] }
    %script{ @objs['bootswatch_js'] }
    %script{ @objs['highlight_js'] }
    :javascript
      hljs.initHighlightingOnLoad();
    %title
      = title
      (hkob-labo)
  %body
    .container-fluid
      .panel.panel-success
        .panel-heading#title
          %h4= title
        .panel-body
EOL

    for line in f
      print "          #{line}"
    end
  end
  print <<"EOL"
        .panel-footer
          .row
            .col-md-5
              %ul.breadcrumb
                %li
                  %a{ @objs['breadcrumb_button_class'].merge(href: '#title') } ↑このページの先頭
                - %w( index.haml history.haml ).each do |url|
                  %li
                    - u, n = link_url_name url, @objs['breadcrumb_button_class']
                    %a{ u }= n
            .col-md-7.text-right
              - tags = (file_info['tags'] || '').split(',').map { |s| s + 'Tags.haml' }
              - if tags.count > 0
                %ul.breadcrumb
                  %li タグ
                  - tags.uniq.each do |link|
                    - u, n = link_url_name link, @objs['breadcrumb_button_class']
                    %li
                      %a{ u }= n
                    - copy_template(link)
          .row
            .col-sm-5
            .col-sm-3
              &copy;2016-2017 小林研究室.
            .col-sm-4
              最終更新日: #{ File::mtime(fname).strftime('%Y年%-m月%-d日　%-H:%M')  }
EOL
end
