require 'yaml'

def relative_path(from, to)
  fromPath = Pathname.new(from).dirname
  toPath = Pathname.new("contents/haml") + to
  toPath.relative_path_from(fromPath).to_s.gsub(/haml/, 'html')
end

def link_url_name(url, option = {})
  hash = @objs['links'][url]
  if hash
    [ option.merge({ href: relative_path(@fname, url)}), hash['name'] ]
  else
    print "site.yml の links[" + url + "] が見つかりません\\n"
    exit(1)
  end
end

def link_url_name_date(url, option = {})
  hash = @objs['links'][url]
  if hash
    [ option.merge({ href: relative_path(@fname, url)}), hash['name'], hash['date'] ]
  else
    print "site.yml の links[" + url + "] が見つかりません\\n"
    exit(1)
  end
end

def link_outer_url_name(key, option = {})
  hash = @objs['outer_links'][key]
  if hash
    [ option.merge({ href: hash['url'] }), hash['name'], hash['desc'] ]
  else
    print "site.yml の outer_links[" + key + "] が見つかりません\\n"
    exit(1)
  end
end

def image_url_alt(imageName, option = {})
  hash = @objs['images'][imageName]
  url = 'images/' + imageName
  if hash
    option.merge({ src: relative_path(@fname, url), class: 'img-thumbnail img-responsive', alt: hash['alt'] })
  else
    print "site.yml の images[" + imageName + "] が見つかりません\\n"
    exit(1)
  end
end

def include_haml(haml, locals = {})
  Haml::Engine.new(File.read('partials/' + haml + '.haml')).render(Object.new, locals)
end

def include_oneliner(oneline, type = :bash)
  [
    %Q(<pre><code lang=") + type.to_s + %Q(">),
    preserve { html_escape(oneline) },
    "</code></pre>"
  ].join('')
end

def include_source(source, type = :ruby)
  include_oneliner File.open(File.join([ 'sources', Pathname.new(@fname_without_base).sub_ext('').to_s, source ])).read, type
end

def nav_tabs(hash)
  include_haml 'nav-tabs', title_hash: hash
end

def sub_figures(array, num)
  hash_array = array.map { |w| w == '-' ? nil : image_url_alt(w) }
  include_haml 'sub_figures', images: hash_array, number: num
end

def tab_pane_class(hash, key)
  active_str = key == hash.keys.first ? "active" : ""
  { class: ("tab-pane face in " + active_str), id: key }
end

def date_str(d)
  d.strftime('%Y年%-m月%-d日　%-H:%M')
end

def date_only_str(d)
  d.strftime('%Y年%-m月%-d日作成')
end

def copy_template(link)
  unless link == 'index.haml'
    fname = "contents/haml/#{link}"
    template = 'partials/template.haml'
    FileUtils.copy_file(template, fname)
    FileUtils.touch(fname)
  end
end
